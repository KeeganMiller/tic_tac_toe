/**********
* Game Manager CPP
* Handles all the game methods
* Keegan Miller | AIE | Tic Tac Toe
***********/

#include "GameManager.h"
#include "Menu.h"
#include <iostream>

// Static Variables declaretion
GameState* GameManager::currentGameState = new Menu();
MatchController* GameManager::currentMatch = nullptr;
bool GameManager::gameRunning = true;

GameManager::~GameManager()
{
	delete GameManager::currentGameState;
}

void GameManager::Init()
{
	Loop();
}

void GameManager::Loop()
{
	while (gameRunning)
	{
		currentGameState->Loop();
	}
}

/// <summary>
/// Changes the current game state
/// <summary>
void GameManager::ChangeGameState(GameState * state)
{
	currentGameState = state;
}

void GameManager::Debug(std::string message, bool waitForInput)
{
	std::cout << message << std::endl;
	
	int input;
	std::cin >> input;
	std::cout << std::endl;
}


