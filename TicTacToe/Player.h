/**********
* Player Header
* Abstract class for both human & AI
* Keegan Miller | AIE | Tic Tac Toe
***********/

#pragma once
#include <string>
class Player
{
public:


	/// <summary>
	/// Construtor for the player
	/// </summary>
	/// <param name="name">Name of the player</param>
	/// <param name="object">Tic Tac Toe Object</param>
	/// <param name="Human">(Default: true)_if this is human or AI</param>
	Player(std::string name, std::string object, bool human = true)
	{
		playersName = name;
		TicTacToeObject = object;
		isHuman = human;
	}

	
	~Player()
	{

	}


	void DrawPlayerUI();			// Draws the players UI

	/// <summary>
	/// Called when the player has won the game
	/// It increments the amount of games the player has won
	/// </summary>
	inline void WonGame() { gamesWon++; }


	virtual void TakeTurn() = 0;
	std::string GetObject() const { return TicTacToeObject; }
	std::string TicTacToeObject;			// Object to add


	
protected:


	std::string playersName;			// Name of the player
	int gamesWon = 0;						// How many games the player has won
	bool isHuman = true;



	
};

