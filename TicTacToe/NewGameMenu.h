#pragma once
#include "GameState.h"

class NewGameMenu : public GameState
{
public:
	/* CONSTRUCTORS */

	NewGameMenu() {}
	~NewGameMenu() {}

	/* METHODS */

	void Draw() override;
	void Loop() override;

	/* PROPERTIES */
private:
	/* METHODS */


	void SetupPlayers();

	/* PROPERTIES */
	int playerSettingUp = 1;			// Which player we are currently setting up
};

