#pragma once
#include "Player.h"
#include <string>
class WinController
{
public:


	/* METHOD */

	static bool CheckWin(Player* player, std::string grid[3][3]);
	
	static bool LeftCol();
	static bool MiddleCol();
	static bool RightCol();
	
	static bool TopRow();
	static bool MiddleRow();
	static bool BottomRow();

private:


	/* PROPERTIES */
	static Player* checkPlayer;
	static std::string grid[3][3];

};

