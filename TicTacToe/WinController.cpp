#include "WinController.h"

Player* WinController::checkPlayer = nullptr;
std::string WinController::grid[3][3] = {};


/// <summary>
/// Checks if the player as won
/// </summary>
/// <param name="player">The player we want to check</param>
/// <returns>If the player won or not</returns>
bool WinController::CheckWin(Player* player, std::string grid[3][3])
{
    // Assign check properties
    WinController::checkPlayer = player;
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            WinController::grid[i][j] = grid[i][j];
        }
    }
    // Check Columns
    if (LeftCol()) return true;
    if (MiddleCol()) return true;
    if (RightCol()) return true;

    // Check Rows
    if (TopRow()) return true;
    if (MiddleRow()) return true;
    if (BottomRow()) return true;
   


    return false;
}

/// <summary>
/// X| |
/// -----
/// X| |
/// -----
/// X| |
/// </summary>
/// <returns></returns>
bool WinController::LeftCol()
{
    if (grid[0][0] == checkPlayer->GetObject() && grid[1][0] == checkPlayer->GetObject() && grid[2][0] == checkPlayer->GetObject())
        return true;

    return false;
}

/// <summary>
///  |X|
/// -----
///  |X|
/// -----
///  |X|
/// </summary>
/// <returns></returns>
bool WinController::MiddleCol()
{
    if (grid[1][0] == checkPlayer->GetObject() && grid[1][1] == checkPlayer->GetObject() && grid[1][2] == checkPlayer->GetObject())
        return true;
    return false;
}

/// <summary>
///  | |X
/// -----
///  | |X
/// -----
///  | |X
/// </summary>
/// <returns></returns>
bool WinController::RightCol()
{
    if (grid[2][0] == checkPlayer->GetObject() && grid[2][1] == checkPlayer->GetObject() && grid[2][2] == checkPlayer->GetObject())
        return true;
    return false;
}

/// <summary>
/// X|X|X
/// -----
///  | |
/// -----
///  | |
/// </summary>
/// <returns></returns>
bool WinController::TopRow()
{
    if (grid[0][0] == checkPlayer->GetObject() && grid[0][1] == checkPlayer->GetObject() && grid[0][2] == checkPlayer->GetObject())
        return true;
    return false;
}


/// <summary>
///  | |
/// -----
/// X|X|X
/// -----
///  | |
/// </summary>
/// <returns></returns>
bool WinController::MiddleRow()
{
    if (grid[1][0] == checkPlayer->GetObject() && grid[1][1] == checkPlayer->GetObject() && grid[1][2] == checkPlayer->GetObject())
        return true;
    return false;
}


/// <summary>
///  | |
/// -----
///  | |
/// -----
/// X|X|X
/// </summary>
/// <returns></returns>
bool WinController::BottomRow()
{
    if (grid[2][0] == checkPlayer->GetObject() && grid[2][1] == checkPlayer->GetObject() && grid[2][2] == checkPlayer->GetObject())
        return true;
    return false;
}
