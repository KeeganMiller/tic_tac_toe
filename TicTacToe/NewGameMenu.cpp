#include "NewGameMenu.h"
#include "GameManager.h"
#include "Player.h"
#include "Human.h"
#include "InGame.h"
#include <iostream>
void NewGameMenu::Draw()
{
	__super::Draw();
	std::cout << "Setup Player " << playerSettingUp << std::endl;
	std::cout << "-------------" << std::endl;
	
}

void NewGameMenu::Loop()
{
	__super::Loop();
	SetupPlayers();
}

/// <summary>
/// Handles setting up the player
/// </summary>
/// <returns></returns>
void NewGameMenu::SetupPlayers()
{

	// Predefine players
	Player* player1 = nullptr;
	Player* player2 = nullptr;

	// Loop 2 times to setup players
	while (playerSettingUp <= 2)
	{
		// Pre-define player properties
		std::string playerName;
		std::string isHuman;
		bool isHumanBool = false;

		// Prompt & Get players name
		std::cout << "Players Name: ";
		std::cin >> playerName; std::cout << std::endl;

		// Prompt & determine if player is AI or human
		std::cout << "Is this player human? (y/n): ";
		std::cin >> isHuman;

		// If they are than set the bool
		if (isHuman == "y" || isHuman == "Y" || isHuman == "Yes" || isHuman == "yes")
		{
			isHumanBool = true;
		}
		else
		{
			isHumanBool = false;
		}

		// If the player is human create a human player depending on which one
		// we are setting up otherwise create AI class
		if (isHumanBool)
		{
			if (playerSettingUp == 1)
			{

				player1 = new Human(playerName, "X");
				
			}
			else
			{
				player2 = new Human(playerName, "O");
				
			}
		}
		else
		{
			// TODO: Implement AI
		}

		playerSettingUp++;
		Draw();
		
	}

	// Set the amount of rounds
	int rounds;
	std::cout << "How many rounds: ";
	std::cin >> rounds;
	std::cout << std::endl;

	// Create the new match controller & assign it
	MatchController* controller = new MatchController(player1, player2, rounds);
	GameManager::SetMatchController(controller);

	// Load the new game state
	GameManager::ChangeGameState(new InGame());
}

