#include "Menu.h"
#include "GameManager.h"
#include "NewGameMenu.h"
#include <iostream>
void Menu::Draw()
{
	__super::Draw();
	std::cout << "TIC TAC TOE" << std::endl;
	std::cout << "------------" << std::endl;
	std::cout << "1 - New Game" << std::endl;
	std::cout << "2 - Continue Game" << std::endl;
	std::cout << "3 - Exit to Desktop" << std::endl;
}

void Menu::Loop()
{
	__super::Loop();
	HandleInput();
}

bool Menu::HandleInput()
{
	std::cout << "------" << std::endl;
	std::cout << "Select a menu option: ";
	
	int input;			// define for the input
	std::cin >> input; std::cout << std::endl;

	switch (input)
	{
	case 1:
		GameManager::ChangeGameState(new NewGameMenu());
		break;
	case 2:
		// TODO: Continue Game
		break;
	case 3:
		// TODO: Quit Game
		break;
	}

	return false;
}
