#pragma once
#include "GameState.h"


class Menu : public GameState
{
public:
	Menu() {}
	~Menu() {}

	void Draw() override;
	void Loop() override;

private:
	void DrawMenu();
	bool HandleInput();

};

